
module.exports = function(req, userType, data){

    return data.map(i => {
        i.__isMock = true;
        return i;
    });

};
